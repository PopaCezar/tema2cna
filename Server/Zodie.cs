﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class Zodiac
    {
        private string zodie;
        private string beginDate;
        private string endDate;

        public Zodiac(string beginDate = "", string endDate = "", string zodie = "")
        {
            this.beginDate = beginDate;
            this.endDate = endDate;
            this.zodie = zodie;
        }

        public string getZodie()
        {
            return zodie;
        }

        public string getBeginDate()
        {
            return beginDate;
        }

        public string getEndDate()
        {
            return endDate;
        }
    }

    public class ListaZodii
    {
        private List<Zodiac> zodieList = new List<Zodiac>();

        public ListaZodii()
        {
            System.IO.StreamReader fileReader = new System.IO.StreamReader("E:/Cloud Computin Projects/Tema2 CNA/Zodiac.txt");
            for (int luna = 0; luna < 12; luna++)
            {
                String line = fileReader.ReadLine();
                string[] date = line.Split(' ');
                       
                Zodiac s = new Zodiac(date[0], date[1], date[2]);
                zodieList.Add(s);
            }
        }

        public string FindSign(string date)
        {
            string[] dateList = date.Split('/');
            int month = int.Parse(dateList[0]);
            int day = int.Parse(dateList[1]);
            foreach (Zodiac zodie in zodieList)
            {
                string[] begin = zodie.getBeginDate().Split('/');
                string[] end = zodie.getEndDate().Split('/');
                if ((month == int.Parse(begin[0]) && day >= int.Parse(begin[1]))
                    || (month == int.Parse(end[0]) && day <= int.Parse(end[1])))
                    return zodie.getZodie();
            }
            return string.Empty;
        }
    }
}
