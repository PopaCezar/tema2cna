﻿using Generated;
using Grpc.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text.RegularExpressions;

namespace Client
{
    class Program
    {
        static bool VerificareData(string data)
        {
            string regex = @"((0?[1-9]|1[0-2])\/(0?[1-9]|[12][0-9]|3[01])\/([1-9][0-9]{0,3}))";
            if (data != string.Empty)
            {
                Match match = Regex.Match(data, regex);
                if (match.Value == data)
                {
                    string[] dataSplit = data.Split('/');
                    int luna = int.Parse(dataSplit[0]);
                    int zi = int.Parse(dataSplit[1]);
                    int an = int.Parse(dataSplit[2]);

                    //Cazul in care luna este Februarie;
                    if (luna == 2)
                    {
                        if (an % 4 != 0)
                        {
                            if (zi > 28)
                                return false;
                        }
                        else
                        {
                            if (an % 100 != 0)
                            {
                                if (zi > 29)
                                    return false;
                            }
                            else if (an % 400 != 0)
                            {
                                if (zi > 28)
                                    return false;
                            }
                            else
                            {
                                if (zi > 29)
                                    return false;
                            }
                        }

                        //if ((an % 4 == 0 && an % 100 != 0 && zi > 29) || (an % 4 != 0 && zi > 28))
                        //    return false;
                    }
                    //Lunile care au 30 de zile respectiv Aprilie, Iunie, Septembrie, Noiembrie;
                    else
                    {
                        if (luna == 4 || luna == 6 || luna == 9 || luna == 11)
                            if (zi > 30)
                                return false;
                    }
                    return true;
                }

            }
            return false;
        }
        static void Main(string[] args)
        {
            const string Host = "localhost";
            const int Port = 16842;

            var channel = new Channel($"{Host}:{Port}", ChannelCredentials.Insecure);

            Console.WriteLine("Write a valid date (mm/dd/yyyy):");

            var date = Console.ReadLine();

            if (VerificareData(date))
            {
                var client = new Generated.ZodiacService.ZodiacServiceClient(channel);
                var output = client.GetZodie(new Generated.Date
                {
                    Date_ = date
                });
                Console.WriteLine("Zodia ta este: {0}.", output.Zodie_);
            }
            else
                Console.WriteLine("Data nu este valida!");

            channel.ShutdownAsync().Wait();
        }
    }
}
